from ipv8.messaging.lazy_payload import VariablePayload, vp_compile
from ipv8.lazy_community import lazy_wrapper
from ipv8.messaging.anonymization.payload import DataPayload

@vp_compile
class TunnelMsg(DataPayload):
    msg_id = 31
    format_list = ['raw']
    names = ["parameters"]

@vp_compile
class DHTUpdate(DataPayload):
    msg_id = 32
    format_list = ['raw']
    names = ["parameters"]


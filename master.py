import os
from asyncio import ensure_future, get_event_loop

import libnacl
import utils
from ipv8.community import Community, DEFAULT_MAX_PEERS
from ipv8.messaging.anonymization.hidden_services import HiddenTunnelCommunity
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
from ipv8.peer import Peer
import ipv8_messages
import grpc
import pickle
import sys
from ipv8.keyvault.public.libnaclkey import LibNaCLPK
from service_spec import service_pb2
from service_spec import service_pb2_grpc
from binascii import unhexlify
import ipv8_messages

# @dataclass(msg_id=1)  # Important note: the message id is cleartext!
# class ExampleMessage:
#     content: bytes

device_name = 'teddy-Lenovo-Legion-Y740-15IRHg'
registry_address = '195.201.197.25:4758'


class LinkEncryptedCommunity(HiddenTunnelCommunity):

    # community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x53'
    # community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x54'
    community_id = unhexlify('a3591a6bd89bbaca0974062a1287afcfbc6fd6bb')

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue = []
        self.received_response = False
        self.once = False
        # self.start()
        self.add_message_handler(ipv8_messages.TunnelMsg,self.tunnelMsg_handler)
        # self.add_message_handler(
            # ipv8_messages.ExampleMessage, self.on_example_message)
        # self.register_task("query", self.query, interval=15.0, delay=0)
        self.register_task("print_peers", self.print_peers, interval=5.0, delay=0)
        ensure_future(self.send())

    # @lazy_wrapper(ipv8_messages.ExampleMessage)
    # async def on_example_message(self, peer, payload):
    #     decrypted = libnacl.public.Box(
    #         self.my_peer.key.key.sk, peer.public_key.key.pk).decrypt(payload.content)
    #     self.received_response = True
    #     print(decrypted.decode('utf-8'))

    # def send_example_message(self, peer, content):
    #     encrypted = libnacl.public.Box(
    #         self.my_peer.key.key.sk, peer.public_key.key.pk).encrypt(content)
    #     self.ez_send(peer, ipv8_messages.ExampleMessage(encrypted))


    @lazy_wrapper(ipv8_messages.TunnelMsg)
    async def tunnelMsg_handler(self,peer,payload):
        print("RECIEVED DHT UPDATE")
        print(payload.parameters)

    async def send(self):
        self.build_tunnels(1)
        def print_circuits():
            print("LENGTH",len(self.find_circuits()))                 
            self.build_tunnels(1)     
            if self.find_circuits():
                print("CIRCUITS ----- ",self.find_circuits())
                print("ADDRESS ----- ",self.find_circuits()[0].peer.address)
                print("Tunneling Data", self.tunnel_data(self.find_circuits()[0],("135.181.222.170",8091), ipv8_messages.TunnelMsg(b"Update")))
       
        self.register_task("print_circuits", print_circuits, interval=10.0, delay=0)

    def print_peers(self):
            print("I am:", self.my_peer, "\nI know:", [str(p) for p in self.get_peers()]) 

    def start(self):
        async def start_communication():
            self.get_peers()
            endpoints = utils.get_endpoints('slave')
            # print(endpoints)

            if endpoints['endpoints']:
                peer_ids = endpoints["peer_ids"]
                ip_addrs = endpoints["endpoints"]
                peer_info = peer_ids[0]
                # print(peer_info)
                # print(peer_info['_address'])
                key = LibNaCLPK(peer_info['key'])
                peer = Peer(key)
                peer.mid = peer_info['mid']
                # peer.public_key = peer_info['public_key']
                peer.public_key.key.pk = peer_info['public_key_key']
                peer._address = peer_info['_address']
                # self.walk_to(peer._address)
                # self.walk_to(("197.156.77.62", "36819"))
                # if (peer):
                print("I am:", self.my_peer, "\nI know:",
                      [str(p) for p in self.get_peers()])
                if(self.get_peers()):
                    if (not self.once):
                        print(">", end=" ")
                        command = input()
                        # print("I am:", self.my_peer, "\nI know:",
                        #     [str(p) for p in self.get_peers()])
                        # for p in self.get_peers():
                        # self.send_example_message(p,str.encode("Hello!"))
                        self.send_example_message(
                            peer, str.encode(command))
                        self.received_response = False

            # self.cancel_pending_task("start_communication")

        self.register_task("start_communication",
                           start_communication, interval=1.5, delay=0)


# async def start_communities():
#     for i in [1]:
#         builder = ConfigBuilder().clear_keys().clear_overlays()
#         builder.add_key("my peer", "curve25519", f"ec{i}.pem")
#         builder.add_overlay("LinkEncryptedCommunity", "my peer", [WalkerDefinition(Strategy.RandomWalk, 10, {'timeout': 3.0})],
#                             default_bootstrap_defs, {}, [('started',)])
#         await IPv8(builder.finalize(), extra_communities={'LinkEncryptedCommunity': LinkEncryptedCommunity}).start()


# ensure_future(start_communities())
# get_event_loop().run_forever()


    async def query(self):
        print("Sending to orchestrator")
        reg = {
            'peer_id': '',
            'ip_addrs': '',
            'services': [],
        }

        service_desc = {}
        service_meta = {}
        peer_info = {}
        addrs = []
        service_meta['name'] = "testing-master"
        service_desc['name'] = "testing-master"
        service_desc['endpoints'] = "addrs"
        service_meta['service_input'] = ''
        service_meta['service_output'] = ''
        service_meta['price'] = 0
        peer_info['nodeID'] = str(device_name)
        peer_info['key'] = self.my_peer.public_key.key_to_bin()
        peer_info['mid'] = self.my_peer.mid
        peer_info['public_key'] = self.my_peer.public_key.key_to_bin()
        peer_info['_address'] = self.my_peer._address
        ip_peer = self.my_peer._address
        peer_info['_address'] = ip_peer
        reg['peer_id'] = peer_info
        reg['ip_addrs'] = ['node_ip', 'adapter_port']
        reg['services'].append(service_meta)

        # if len(reg['services']) == 0:
        #     with grpc.insecure_channel(str(registry_address)) as channel:
        #         stub = service_pb2_grpc.RegistryStub(channel)
        #         services = pickle.dumps({})
        #         try:
        #             if ip_peer != None and "UDPv4Address(ip='192" not in str(ip_peer) and "UDPv4Address(ip='127" not in str(ip_peer) and "UDPv4Address(ip='172" not in str(ip_peer) and "UDPv4Address(ip='10." not in str(ip_peer):
        #                 response = stub.updateRegistry(
        #                     service_pb2.Services(services_info=services))
        #         except Exception as e:
        #             logger.error(e)
        # else:
        #     if len(self.routing_tables) > 0:
        #         if len(self.routing_tables[UDPv4Address].trie.root.value.nodes.values()) > 0:
        #             for node in list(self.routing_tables[UDPv4Address].trie.root.value.nodes.values()):
        #                 encoded_result = encode_message(reg)
        #                 logger.info(node)
        #                 self.ez_send(
        #                     node, ipv8_messages.PeerInfo(encoded_result))
        #     else:
        #         logger.info("Routing table is empty")

        # Updates global orchestrator for automatic service deployment
        channel = grpc.insecure_channel(str(registry_address))
        stub = service_pb2_grpc.RegistryStub(channel)
        services = pickle.dumps(reg)
        # try:
        # if ip_peer != None and "UDPv4Address(ip='192" not in str(ip_peer) and "UDPv4Address(ip='127" not in str(ip_peer) and "UDPv4Address(ip='172" not in str(ip_peer) and "UDPv4Address(ip='10." not in str(ip_peer):
        response = stub.updateRegistry(
            service_pb2.Services(services_info=services))
        # except Exception as e:
        # logger.error(e)
        print("Finished Sending to orchestrator")
        # print(reg)

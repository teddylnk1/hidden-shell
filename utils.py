import grpc
import pickle
from service_spec import service_pb2
from service_spec import service_pb2_grpc


registry_address = '195.201.197.25:4758'


def get_endpoints(service_name):
    with grpc.insecure_channel(str(registry_address)) as channel:
        stub = service_pb2_grpc.RegistryStub(channel)
        response = stub.reqServiceEndpoints(
            service_pb2.ServiceRequest(service_name=service_name))
        endpoints = pickle.loads(response.endpoints)

    return endpoints
